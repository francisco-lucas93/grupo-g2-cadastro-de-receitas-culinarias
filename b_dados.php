<?php
session_start();
include_once ("conexao.php");


//Cargo
	$descricao = filter_input (INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
	
	$result_cargo = "INSERT INTO mvgv_cargo (descricao) VALUES ('$descricao')";
	$resultado_cargo = mysqli_query ($conn, $result_cargo);

	if (mysqli_insert_id($conn)){
	$_SESSION['msg_car'] = "<p style='color:green;'>Usuário cadastrado com sucesso </p>";
	header ("Location: cargo.php");
	}else{
	$_SESSION['msg_car'] = "<p style='color:red;'>Usuário não cadastrado</p>";
	header("Location: cargo.php");
	}
	
//Funcionário
	$nome = filter_input (INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
	$rg = filter_input (INPUT_POST, 'rg', FILTER_SANITIZE_STRING);
	$data_adm = filter_input (INPUT_POST, 'data_adm', FILTER_SANITIZE_NUMBER_FLOAT);
	$data_demissao = filter_input (INPUT_POST, 'data_demissao', FILTER_SANITIZE_NUMBER_FLOAT);
	$salario = filter_input (INPUT_POST, 'salario', FILTER_SANITIZE_STRING);
	$Cargo = filter_input (INPUT_POST, 'Cargo', FILTER_SANITIZE_STRING);
	$nome_fantasia = filter_input (INPUT_POST, 'nome_fantasia', FILTER_SANITIZE_STRING);
	
	$result_funcionario = "INSERT INTO mvgv_funcionario (nome, rg, data_adm, data_demissao, salario, Cargo, nome_fantasia) VALUES ('$nome', '$rg', '$data_adm', '$data_demissao', '$salario', '$Cargo', '$nome_fantasia')";
	$resultado_funcionario = mysqli_query ($conn, $result_funcionario);
	
	if (mysqli_insert_id($conn)){
	$_SESSION['msg_fun'] = "<p style='color:green;'>Funcionário cadastrado com sucesso </p>";
	header ("Location: funcionario.php");
	}else{
	$_SESSION['msg_fun'] = "<p style='color:red;'>Funcionário não cadastrado</p>";
	header("Location: funcionario.php");
	}
	
//Categoria
	$descricao_cat = filter_input (INPUT_POST, 'descricao_cat', FILTER_SANITIZE_STRING);	
	
	$result_categoria = "INSERT INTO mvgv_categoria (descricao_cat) VALUES ('$descricao_cat')";
	$resultado_categoria = mysqli_query ($conn, $result_categoria);

//	if (mysqli_insert_id($conn)){
//	$_SESSION['msg_cat'] = "<p style='color:green;'>Alimento cadastrado com sucesso </p>";
//	header ("Location: categoria.php");
//	}else{
//	$_SESSION['msg_cat'] = "<p style='color:red;'>Alimento não cadastrado</p>";
//	header("Location: categoria.php");
//	}
	
//Ingrediente
	$nome_ing = filter_input (INPUT_POST, 'nome_ing', FILTER_SANITIZE_STRING);	
	$descricao_ing = filter_input (INPUT_POST, 'descricao_ing', FILTER_SANITIZE_STRING);	
	
	$result_ingrediente = "INSERT INTO mvgv_ingrediente (nome_ing, descricao_ing) VALUES ('$nome_ing', '$descricao_ing')";
	$resultado_ingrediente = mysqli_query ($conn, $result_ingrediente);
	
//	if (mysqli_insert_id($conn)){
//	$_SESSION['msg_ing'] = "<p style='color:green;'>Alimento cadastrado com sucesso </p>";
//	header ("Location: ingrediente.php");
//	}else{
//	$_SESSION['msg_ing'] = "<p style='color:red;'>Alimento não cadastrado</p>";
//	header("Location: ingrediente.php");
//	}

//Livro
	$titulo = filter_input (INPUT_POST, 'titulo', FILTER_SANITIZE_STRING);
	$isbn = filter_input (INPUT_POST, 'isbn', FILTER_SANITIZE_STRING);
	$editor = filter_input (INPUT_POST, 'editor', FILTER_SANITIZE_STRING);
	
	$result_livro = "INSERT INTO mvgv_livro (titulo, isbn, editor) VALUES ('$titulo', '$isbn', '$editor')";
	$resultado_livro = mysqli_query ($conn, $result_livro);	
	
//	if (mysqli_insert_id($conn)){
//	$_SESSION['msg_liv'] = "<p style='color:green;'>Livro cadastrado com sucesso </p>";
//	header ("Location: livro.php");
//	}else{
//	$_SESSION['msg_liv'] = "<p style='color:red;'>Livro não cadastrado</p>";
//	header("Location: livro.php");
//	}

//Medida
	$descricao_med = filter_input (INPUT_POST, 'descricao_med', FILTER_SANITIZE_STRING);
	
	$result_medida = "INSERT INTO mvgv_medida (descricao_med) VALUES ('$descricao_med')";
	$resultado_medida = mysqli_query ($conn, $result_medida);	
	
//	if (mysqli_insert_id($conn)){
//	$_SESSION['msg_med'] = "<p style='color:green;'>Medida cadastrada com sucesso </p>";
//	header ("Location: medida.php");
//	}else{
//	$_SESSION['msg_med'] = "<p style='color:red;'>Medida não cadastrada</p>";
//	header("Location: medida.php");
//	}

//Receita
	$nome_rec = filter_input (INPUT_POST, 'nome_rec', FILTER_SANITIZE_STRING);
	$coz_elaborador = filter_input (INPUT_POST, 'coz_elaborador', FILTER_SANITIZE_STRING);
	$data_criacao = filter_input (INPUT_POST, 'data_criacao', FILTER_SANITIZE_NUMBER_FLOAT);
	$data_Categoria = filter_input (INPUT_POST, 'Categoria', FILTER_SANITIZE_NUMBER_FLOAT);
	$modo_preparo = filter_input (INPUT_POST, 'modo_preparo', FILTER_SANITIZE_STRING);
	$qtde_porcao = filter_input (INPUT_POST, 'qtde_porcao', FILTER_SANITIZE_STRING);
	$nome_degustador = filter_input (INPUT_POST, 'nome_degustador', FILTER_SANITIZE_STRING);
	$nome_data_degustacao = filter_input (INPUT_POST, 'data_degustacao', FILTER_SANITIZE_STRING);
	$nome_nota_degustacao = filter_input (INPUT_POST, 'nota_degustacao', FILTER_SANITIZE_STRING);
	$nome_ind_inedita = filter_input (INPUT_POST, 'ind_inedita', FILTER_SANITIZE_STRING);
	
	$result_receita = "INSERT INTO mvgv_funcionario (nome_rec, coz_elaborador, data_criacao, Categoria, modo_preparo, qtde_porcao, nome_degustador, data_degustacao, nota_degustacao, ind_inedita)  VALUES ('$nome_rec', '$coz_elaborador', '$data_criacao', '$Categoria', '$modo_preparo', '$qtde_porcao', '$nome_degustador', '$data_degustacao, '$nota_degustacao, '$ind_inedita')";
	$resultado_receita = mysqli_query ($conn, $result_receita);
	
//	if (mysqli_insert_id($conn)){
//	$_SESSION['msg_rec'] = "<p style='color:green;'>Receita cadastrada com sucesso </p>";
//	header ("Location: funcionario.php");
//	}else{
//	$_SESSION['msg_rec'] = "<p style='color:red;'>Receita não cadastrada</p>";
//	header("Location: funcionario.php");
//	}

//Receita_ingrediente_medida
	$nome_rec_med = filter_input (INPUT_POST, 'nome_rec_med', FILTER_SANITIZE_STRING);
	$coz_elaborador_rec = filter_input (INPUT_POST, 'coz_elaborador_rec', FILTER_SANITIZE_STRING);
	$idIngred_rec = filter_input (INPUT_POST, 'idIngred_rec', FILTER_SANITIZE_NUMBER_FLOAT);
	$qtde = filter_input (INPUT_POST, 'qtde', FILTER_SANITIZE_NUMBER_FLOAT);
	$idMedida = filter_input (INPUT_POST, 'idMedida', FILTER_SANITIZE_STRING);
	
	$result_receita_ingrediente_medida = "INSERT INTO mvgv_receita_ingrediente_medida (nome_rec_med, coz_elaborador_rec, idIngred_rec, qtde, idMedida)  VALUES ('$nome_rec_med', '$coz_elaborador_rec', '$idIngred_rec', '$qtde', '$idMedida')";
	$resultado_receita_ingrediente_medida = mysqli_query ($conn, $result_receita_ingrediente_medida);
	
//	if (mysqli_insert_id($conn)){
//	$_SESSION['msg_rec_ing'] = "<p style='color:green;'>Receita cadastrada com sucesso </p>";
//	header ("Location: receita_ingrediente_medida.php");
//	}else{
//	$_SESSION['msg_rec_ing'] = "<p style='color:red;'>Receita não cadastrada</p>";
//	header("Location: receita_ingrediente_medida.php");
//	}

//Receita_publicada //Verificar colunas, atributos e inserts
	$idLivro = filter_input (INPUT_POST, 'idLivro', FILTER_SANITIZE_STRING);
	$nome = filter_input (INPUT_POST, 'nome_rec_med', FILTER_SANITIZE_STRING);
	$coz_elaborador = filter_input (INPUT_POST, 'coz_elaborador', FILTER_SANITIZE_STRING);
	
	$result_receita_publicada = "INSERT INTO mvgv_receita_publicada (idLivro, nome, coz_elaborador)  VALUES ('$idLivro', '$nome', '$coz_elaborador')";
	$resultado_receita_publicada = mysqli_query ($conn, $result_receita_publicada);
	
//	if (mysqli_insert_id($conn)){
//	$_SESSION['msg_rec_pub'] = "<p style='color:green;'>Receita cadastrada com sucesso </p>";
//	header ("Location: receita_publicada.php");
//	}else{
//	$_SESSION['msg_rec_pub'] = "<p style='color:red;'>Receita não cadastrada</p>";
//	header("Location: receita_publicada.php");
//	}

//Referência
	$idFunc = filter_input (INPUT_POST, 'idFunc', FILTER_SANITIZE_STRING);
	$idRestaurante = filter_input (INPUT_POST, 'idRestaurante', FILTER_SANITIZE_STRING);
	$data_inicio = filter_input (INPUT_POST, 'data_inicio', FILTER_SANITIZE_STRING);
	$data_fim = filter_input (INPUT_POST, 'data_fim', FILTER_SANITIZE_STRING);
	
	$result_referencia = "INSERT INTO mvgv_referencia (idFunc, idRestaurante, data_inicio, data_fim)  VALUES ('$idFunc', '$idRestaurante', '$data_inicio', '$data_fim')";
	$resultado_referencia = mysqli_query ($conn, $result_referencia);
	
//	if (mysqli_insert_id($conn)){
//	$_SESSION['msg_ref'] = "<p style='color:green;'>Referência cadastrada com sucesso </p>";
//	header ("Location: referencia.php");
//	}else{
//	$_SESSION['msg_ref'] = "<p style='color:red;'>Referência não cadastrada</p>";
//	header("Location: referencia.php");
//	}
	
//Restaurante
	$nome_res = filter_input (INPUT_POST, 'nome_res', FILTER_SANITIZE_STRING);

	$result_restaurante = "INSERT INTO mvgv_restaurante (nome_res) VALUES ('$nome_res')";
	$resultado_restaurante = mysqli_query ($conn, $result_restaurante);
	

	
//echo "Nome: $descricao <br>";	