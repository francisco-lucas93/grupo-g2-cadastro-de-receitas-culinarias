<?php
session_start();

?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
	<meta charset"UTF-8">
        <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--CSS MATERIALIZE-->
      <link rel="stylesheet" href="materialize/css/materialize.min.css">
       <title>Cargo</title>
		<link rel="icon" href="logochef.png">		
	</head>
	<body>
	<nav class="blue-grey">
        <div class="nav-wrapper container">
            <div class="brand-logo light"></div>
            <ul class="right">
			
				<li><a href="http://localhost/grupog2/index.php">HOME</a></li>
				<li><a href="http://localhost/grupog2/cargo.php">Cargo</a></li>
				<li><a href="http://localhost/grupog2/funcionario.php">Funcionário</a></li>
				<li><a href="http://localhost/grupog2/categoria.php">Categoria</a></li>
				<li><a href="http://localhost/grupog2/ingrediente.php">Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/livro.php">Livro</a></li>
				<li><a href="http://localhost/grupog2/medida.php">Medida</a></li>
				<li><a href="http://localhost/grupog2/receita.php">Receita</a></li>
				<li><a href="http://localhost/grupog2/receita_ingrediente_medida.php">Receita Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/receita_publicada.php">Receita Publicada</a></li>
				<li><a href="http://localhost/grupog2/referencia.php">Referência</a></li>
				<li><a href="http://localhost/grupog2/restaurante.php">Restaurante</a></li>
				</ul>
</div>
</nav>

<div class="row container">
    <p>&nbsp;</p>   
		
	<form method="POST" action="b_dados_cargo.php" class="col s12">		
		<fieldset class="cargo">
            <legend><img src="imagens/logochef.png" alt="[imagem]" width="100"></legend>
            <h5 class="light center">Cadastrar Cargo</h5>

			<div class="input-field col s12">
			<input type="text" name="descricao" placeholder="Digite o cargo" maxlength="40">
</div>
			

			<br><br>			
			<br>
			<div class="input-field col s12">
                <input type="submit" value="Cadastrar" class="btn green">
                <input type="submit" value="Limpar" class="btn red">
			</div>
</fieldset>
		</form>



		<script type="text/javascript" scr="materialize/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" scr="materialize/js/materialize.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

    });


</script>

		<?php
		if(isset($_SESSION['msg_car'])){
			echo $_SESSION['msg_car'];
			unset($_SESSION['msg_car']);
		}
		?>
		
		<br>
			
		<?php
		include_once("conexao.php");
		$result_cargos = "SELECT * FROM mvgv_cargo";
		$resultado_cargos = mysqli_query($conn, $result_cargos);
		?>
		<div class="page-header">
		<br>
		<br>
		<br>
		</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table" border="12">
						<thead>
							<tr>
								<th>ID</th>
								<th>Cargo</th>
								<th>Ação</th
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php while($rows_cargos = mysqli_fetch_assoc($resultado_cargos)){ ?>
								<tr>
									<td><?php echo $rows_cargos ['idCargo']; ?></td>
									<td><?php echo $rows_cargos ['descricao']; ?></td>
									<td>
										<a href="cargo_del.php?delete=<?php echo $rows_cargos['idCargo']; ?>"
										class="btn btn-xs btn-primary">Delete</a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>	
				</div>
			</div>
		<div><a href="http://localhost/grupog2/cargo_relatorio_pdf.php">Baixar relatório em PDF</a></div>
	</center></body>
</html>