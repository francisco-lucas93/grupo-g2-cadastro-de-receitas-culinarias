<?php 
session_start();

?>
<html>
    <head>
        <meta charset"UTF-8">
        <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--CSS MATERIALIZE-->
      <link rel="stylesheet" href="materialize/css/materialize.min.css">
        <title>Funcionário</title>
		<link rel="icon" href="logochef.png">
    </head>

    <body>

    <nav class="blue-grey">
        <div class="nav-wrapper container">
            <div class="brand-logo light"></div>
            <ul class="right">
			
				<li><a href="http://localhost/grupog2/index.php">HOME</a></li>
				<li><a href="http://localhost/grupog2/cargo.php">Cargo</a></li>
				<li><a href="http://localhost/grupog2/funcionario.php">Funcionário</a></li>
				<li><a href="http://localhost/grupog2/categoria.php">Categoria</a></li>
				<li><a href="http://localhost/grupog2/ingrediente.php">Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/livro.php">Livro</a></li>
				<li><a href="http://localhost/grupog2/medida.php">Medida</a></li>
				<li><a href="http://localhost/grupog2/receita.php">Receita</a></li>
				<li><a href="http://localhost/grupog2/receita_ingrediente_medida.php">Receita Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/receita_publicada.php">Receita Publicada</a></li>
				<li><a href="http://localhost/grupog2/referencia.php">Referência</a></li>
				<li><a href="http://localhost/grupog2/restaurante.php">Restaurante</a></li>
				</ul>
</div>
</nav>

<div class="row container">
    <p>&nbsp;</p>   
	<form method="POST" action="b_dados_funcionario.php" class="col s12">		
		<fieldset class="funcionario">
            <legend><img src="imagens/logochef.png" alt="[imagem]" width="100"></legend>
            <h5 class="light center">Cadastrar Funcionário</h5>


			<div class="input-field col s12">
			<input type="text" name="nome" placeholder="Digite o nome"><br><br>
			</div>

			<div class="input-field col s12">
			<input type="text" name="rg" placeholder="Digite o RG"><br><br>
			</div>

			<div class="input-field col s12">
			<label>Data de admissão: </label>
			</div>
			<div class="input-field col s12">
			<input type="date" value="" id="data_adm" placeholder="Digite a data de admissão"><br><br>
			</div>

			<div class="input-field col s12">
			<label>Data de demissão: </label>
			</div>

			<div class="input-field col s12">
			
			<input type="date" id="data_demissao"  placeholder="Digite a data de demissão"><br><br>
			</div>

			<div class="input-field col s12">
			
			<input type="number" id="salario" min="0.00" max="1000000.00" step="0.01" / placeholder="Digite o salário"><br><br>
			</div>

			<div class="input-field col s12">
			
			<input type="text" name="Cargo" placeholder="Digite o cargo"><br><br>
			</div>

			<div class="input-field col s12">
			
			
			<input type="text" name="nome_fantasia" placeholder="Digite o nome fantasia do empregado">
			</div><br><br>
			<?php
			if(isset($_SESSION['msg'])){
				echo $_SESSION['msg'];
				unset($_SESSION['msg']);
			}
			?><br>
			<div class="input-field col s12">
                <input type="submit" value="Cadastrar" class="btn green">
                <input type="submit" value="Limpar" class="btn red">
			</div>
			</fieldset>
		</form>


		<script type="text/javascript" scr="materialize/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" scr="materialize/js/materialize.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

    });


</script>
		
		<br><br>
		<?php
		include_once("conexao.php");
		$result_funcionario = "SELECT * FROM mvgv_funcionario";
		$resultado_funcionario = mysqli_query($conn, $result_funcionario);
		?>
		<div class="page-header">
		<br>
		</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table" border="1">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nome</th>
								<th>RG</th>
								<th>Data de Admissão</th>
								<th>Data de Demissão</th>
								<th>Salário</th>
								<th>Cargo</th>
								<th>Nome Fantasia</th>
								<th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<?php while($rows_funcionario = mysqli_fetch_assoc($resultado_funcionario)){ ?>
								<tr>
									<td><?php echo $rows_funcionario ['idFunc']; ?></td>
									<td><?php echo $rows_funcionario ['nome']; ?></td>
									<td><?php echo $rows_funcionario ['rg']; ?></td>
									<td><?php echo $rows_funcionario ['data_adm']; ?></td>
									<td><?php echo $rows_funcionario ['data_demissao']; ?></td>
									<td><?php echo $rows_funcionario ['salario']; ?></td>
									<td><?php echo $rows_funcionario ['Cargo']; ?></td>
									<td><?php echo $rows_funcionario ['nome_fantasia']; ?></td>
									<td>
										<button type="button" class="btn btn-xs btn-primary">Visualizar</button>
										<button type="button" class="btn btn-xs btn-warning">Editar</button>
										<button type="button" class="btn btn-xs btn-danger">Apagar</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</center></body>
</html>