<?php
session_start();
include_once ("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Funcionário</title>		
	</head>
	<body><center>
		<br>
		<br>
		<table border="0">
		<tr>
			<td><a href="http://localhost/grupog2/index.php">HOME</a></td><td></td>
			<td><a href="http://localhost/grupog2/cargo.php">Cargo</a></td><td></td>
			<td><a href="http://localhost/grupog2/funcionario.php">Funcionário</a></td><td></td>
			<td><a href="http://localhost/grupog2/categoria.php">Categoria</a></td><td></td>
			<td><a href="http://localhost/grupog2/ingrediente.php">Ingrediente</a></td><td></td>
			<td><a href="http://localhost/grupog2/livro.php">Livro</a></td><td></td>
			<td><a href="http://localhost/grupog2/medida.php">Medida</a></td><td></td>
			<td><a href="http://localhost/grupog2/receita.php">Receita</a></td><td></td>
			<td><a href="http://localhost/grupog2/receita_ingrediente_medida.php">Receita Ingrediente</a></td><td></td>
			<td><a href="http://localhost/grupog2/receita_publicada.php">Receita Publicada</a></td><td></td>
			<td><a href="http://localhost/grupog2/referencia.php">Referência</a></td><td></td>
			<td><a href="http://localhost/grupog2/restaurante.php">Restaurante</a></td><td></td>
		</tr>
		</table>
	<br>
	<br>
		<h1>Funcionário</h1>
		<form method="POST" action="b_dados_funcionario.php">
			<label>Nome: </label>
			<input type="text" name="nome" placeholder="Digite o nome"><br><br>
			
			<label>Registro Geral: </label>
			<input type="text" name="rg" placeholder="Digite o RG"><br><br>
			
			<label>Data de admissão: </label>
			<input type="date" id="data_adm" placeholder="Digite a data de admissão"><br><br>
			
			<label>Data de demissão: </label>
			<input type="date" id="data_demissao"  placeholder="Digite a data de demissão"><br><br>
			
			<label>Salário: </label>
			<input type="number" id="salario" min="0.00" max="1000000.00" step="0.01" / placeholder="Digite o salário"><br><br>
			
			<label>Cargo: </label>
			<select name="Cargo">
			<option>Selecione o cargo</option>
			<?php
				session_start();
				include_once("conexao.php");
				$result_Cargo = "SELECT * FROM mvgv_cargo";
				$resultado_Cargo = mysqli_query	($conn, $result_Cargo);
				while($row_Cargo = mysqli_fetch_assoc	($resultado_Cargo)){ ?>
				<option value="<?php echo $row_Cargo ['descricao']; ?>"><?php echo $row_Cargo ['descricao']; ?>
				</option> <?php
					
				}
			?>
			</select>
			<br><br>
			
			<label>Nome Fantasia: </label>
			<input type="text" name="nome_fantasia" placeholder="Digite o nome fantasia do empregado"><br><br>
			<br>
			<input type="submit" value="Cadastrar">
		</form>
		<?php
			if(isset($_SESSION['msg'])){
				echo $_SESSION['msg'];
				unset($_SESSION['msg']);
			}
			?>
		<br><br>
		<?php
		include_once("conexao.php");
		$result_funcionario = "SELECT * FROM mvgv_funcionario";
		$resultado_funcionario = mysqli_query($conn, $result_funcionario);
		?>
		<div class="page-header">
		<br>
		</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table" border="1">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nome</th>
								<th>RG</th>
								<th>Data de Admissão</th>
								<th>Data de Demissão</th>
								<th>Salário</th>
								<th>Cargo</th>
								<th>Nome Fantasia</th>
								<th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<?php while($rows_funcionario = mysqli_fetch_assoc($resultado_funcionario)){ ?>
								<tr>
									<td><?php echo $rows_funcionario ['idFunc']; ?></td>
									<td><?php echo $rows_funcionario ['nome']; ?></td>
									<td><?php echo $rows_funcionario ['rg']; ?></td>
									<td><?php echo $rows_funcionario ['data_adm']; ?></td>
									<td><?php echo $rows_funcionario ['data_demissao']; ?></td>
									<td><?php echo $rows_funcionario ['salario']; ?></td>
									<td><?php echo $rows_funcionario ['Cargo']; ?></td>
									<td><?php echo $rows_funcionario ['nome_fantasia']; ?></td>
									<td>
										<button type="button" class="btn btn-xs btn-primary">Visualizar</button>
										<button type="button" class="btn btn-xs btn-warning">Editar</button>
										<button type="button" class="btn btn-xs btn-danger">Apagar</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</center></body>
</html>