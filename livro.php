<?php 
session_start();
?>
<html>
    <head>
        <meta charset"UTF-8">
        <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--CSS MATERIALIZE-->
      <link rel="stylesheet" href="materialize/css/materialize.min.css">
        <title></title>
    </head>

    <body>

    <nav class="blue-grey">
        <div class="nav-wrapper container">
            <div class="brand-logo light"></div>
            <ul class="right">
			
				<li><a href="http://localhost/grupog2/index.php">HOME</a></li>
				<li><a href="http://localhost/grupog2/cargo.php">Cargo</a></li>
				<li><a href="http://localhost/grupog2/funcionario.php">Funcionário</a></li>
				<li><a href="http://localhost/grupog2/categoria.php">Categoria</a></li>
				<li><a href="http://localhost/grupog2/ingrediente.php">Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/livro.php">Livro</a></li>
				<li><a href="http://localhost/grupog2/medida.php">Medida</a></li>
				<li><a href="http://localhost/grupog2/receita.php">Receita</a></li>
				<li><a href="http://localhost/grupog2/receita_ingrediente_medida.php">Receita Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/receita_publicada.php">Receita Publicada</a></li>
				<li><a href="http://localhost/grupog2/referencia.php">Referência</a></li>
				<li><a href="http://localhost/grupog2/restaurante.php">Restaurante</a></li>
				</ul>
</div>
</nav>

<div class="row container">
    <p>&nbsp;</p>  
	<form method="POST" action="b_dados.php">
				
		<fieldset class="ingrediente">
		<legend><img src="imagens/logochef.png" alt="[imagem]" width="100"></legend>
            <h5 class="light center">Cadastrar Livro</h5>
			
			<div class="input-field col s12">
			<input type="text" name="titulo" placeholder="Digite o título do livro"><br><br>
			</div>
			
			<div class="input-field col s12">
			<input type="text" name="isbn" placeholder="Digite o International Standard Book Number"><br><br>
			</div>


			<div class="input-field col s12">
			<input type="text" name="editor" placeholder="Digite o nome do editor"><br><br>
			</div><p>&nbsp;</p> 
			
			
            <div class="input-field col s12">
                <input type="submit" value="Cadastrar" class="btn green">
                <input type="submit" value="Limpar" class="btn red">
			</div>
	</div>


</fieldset>
</form>
<script type="text/javascript" scr="materialize/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" scr="materialize/js/materialize.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

    });


		
		</script>
<?php
	if(isset($_SESSION['msg_liv'])){
	echo $_SESSION['msg_liv'];
	unset($_SESSION['msg_liv']);
	}
?>

		<br>
		
		<?php
		include_once("conexao.php");
		$result_livro = "SELECT * FROM mvgv_livro";
		$resultado_livro = mysqli_query($conn, $result_livro);
		?>
		<div class="page-header">
		<br>
		</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table" border="1">
						<thead>
							<tr>
								<th>ID</th>
								<th>Título</th>
								<th>ISBN</th>
								<th>Editor</th>
								<th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<?php while($rows_livro = mysqli_fetch_assoc($resultado_livro)){ ?>
								<tr>
									<td><?php echo $rows_livro ['idLivro']; ?></td>
									<td><?php echo $rows_livro ['titulo']; ?></td>
									<td><?php echo $rows_livro ['isbn']; ?></td>
									<td><?php echo $rows_livro ['editor']; ?></td>
									<td>
										<button type="button" class="btn btn-xs btn-primary">Visualizar</button>
										<button type="button" class="btn btn-xs btn-warning">Editar</button>
										<button type="button" class="btn btn-xs btn-danger">Apagar</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>	
				</div>
			</div>
		</center>	
	</body>
</html>