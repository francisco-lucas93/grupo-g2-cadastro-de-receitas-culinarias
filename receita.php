<?php 
session_start();
?>
<html>
    <head>
        <meta charset"UTF-8">
        <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--CSS MATERIALIZE-->
      <link rel="stylesheet" href="materialize/css/materialize.min.css">
        <title></title>
    </head>

    <body>

    <nav class="blue-grey">
        <div class="nav-wrapper container">
            <div class="brand-logo light"></div>
            <ul class="right">
			
				<li><a href="http://localhost/grupog2/index.php">HOME</a></li>
				<li><a href="http://localhost/grupog2/cargo.php">Cargo</a></li>
				<li><a href="http://localhost/grupog2/funcionario.php">Funcionário</a></li>
				<li><a href="http://localhost/grupog2/categoria.php">Categoria</a></li>
				<li><a href="http://localhost/grupog2/ingrediente.php">Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/livro.php">Livro</a></li>
				<li><a href="http://localhost/grupog2/medida.php">Medida</a></li>
				<li><a href="http://localhost/grupog2/receita.php">Receita</a></li>
				<li><a href="http://localhost/grupog2/receita_ingrediente_medida.php">Receita Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/receita_publicada.php">Receita Publicada</a></li>
				<li><a href="http://localhost/grupog2/referencia.php">Referência</a></li>
				<li><a href="http://localhost/grupog2/restaurante.php">Restaurante</a></li>
				</ul>
</div>
</nav>

<div class="row container">
    <p>&nbsp;</p>  
	<form method="POST" action="b_dados.php">>
		<fieldset class="ingrediente">
		<legend><img src="imagens/logochef.png" alt="[imagem]" width="100"></legend>
            <h5 class="light center">Cadastrar Receita</h5>
			
		<div class="input-field col s12">
			<input type="text" name="nome_rec" placeholder="Digite o nome da receita"><br><br>
			</div>
			
			<div class="input-field col s12">
			<input type="text" name="coz_elaborador" placeholder="Digite o nome do cozinheiro elaborador"><br><br>
			</div>

			<div class="input-field col s12">
			<label>Data de criação: </label>
</div>
			<div class="input-field col s12">
			<input type="date" name="data_criacao" placeholder="Digite a data de criação"><br><br>
</div>

			<div class="input-field col s12">
			<input type="text" name="Categoria" placeholder="Digite a categoria da receita"><br><br>
</div>

			<div class="input-field col s12">
			<input type="text" name="modo_preparo" placeholder="Digite o modo de preparo"><br><br>
</div>

			<div class="input-field col s12">
			<input type="number" name="qtde_porcao" min="0.00" max="10000.00" step="0.01" / placeholder="Digite a quantidade de porções"><br><br>
</div>

			<div class="input-field col s12">
			<input type="text" name="degustador" placeholder="Digite o nome do degustador"><br><br>
</div>

<div class="input-field col s12">
			<label>Data da Degustação: </label>
</div>
			<div class="input-field col s12">
			<input type="date" name="data_degustacao" placeholder="Digite a data da degustação"><br><br>
</div>

			<div class="input-field col s12">
			<input type="number" name="nota_degustacao" min="0.00" max="10.0" step="0.01" / placeholder="Digite a nota da degustação"><br><br>
			</div><p>&nbsp;</p> 
			
			
            <div class="input-field col s12">
                <input type="submit" value="Cadastrar" class="btn green">
                <input type="submit" value="Limpar" class="btn red">
			</div>
	</div>


</fieldset>
</form>
<script type="text/javascript" scr="materialize/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" scr="materialize/js/materialize.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

    });


		
		</script>
		<br>
		<?php
		include_once("conexao.php");
		$result_receita = "SELECT * FROM mvgv_receita";
		$resultado_receita = mysqli_query($conn, $result_receita);
		?>
		<div class="page-header">
		<br>
		</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table" border="1">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Cozinheiro</th>
								<th>ID</th>
								<th>Data de criação</th>
								<th>Categoria</th>
								<th>Modo de preparo</th>
								<th>Porções</th>
								<th>Degustador</th>
								<th>Data de degustação</th>
								<th>Nota de degustação</th>
								<th>Receita inédita</th>
							</tr>
						</thead>
						<tbody>
							<?php while($rows_receita = mysqli_fetch_assoc($resultado_receita)){ ?>
								<tr>
									<td><?php echo $rows_receita ['nome_rec']; ?></td>
									<td><?php echo $rows_receita ['coz_elaborador']; ?></td>
									<td><?php echo $rows_receita ['idReceita']; ?></td>
									<td><?php echo $rows_receita ['data_criacao']; ?></td>
									<td><?php echo $rows_receita ['Categoria']; ?></td>
									<td><?php echo $rows_receita ['modo_preparo']; ?></td>
									<td><?php echo $rows_receita ['qtde_porcao']; ?></td>
									<td><?php echo $rows_receita ['degustador']; ?></td>
									<td><?php echo $rows_receita ['data_degustacao']; ?></td>
									<td><?php echo $rows_receita ['nota_degustacao']; ?></td>
									<td><?php echo $rows_receita ['ind_inedita']; ?></td>
									<td>
										<button type="button" class="btn btn-xs btn-primary">Visualizar</button>
										<button type="button" class="btn btn-xs btn-warning">Editar</button>
										<button type="button" class="btn btn-xs btn-danger">Apagar</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</center>	
	</body>
</html>