<?php 
session_start();
?>
<html>
    <head>
        <meta charset"UTF-8">
        <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--CSS MATERIALIZE-->
      <link rel="stylesheet" href="materialize/css/materialize.min.css">
        <title></title>
    </head>

    <body>

    <nav class="blue-grey">
        <div class="nav-wrapper container">
            <div class="brand-logo light"></div>
            <ul class="right">
			
				<li><a href="http://localhost/grupog2/index.php">HOME</a></li>
				<li><a href="http://localhost/grupog2/cargo.php">Cargo</a></li>
				<li><a href="http://localhost/grupog2/funcionario.php">Funcionário</a></li>
				<li><a href="http://localhost/grupog2/categoria.php">Categoria</a></li>
				<li><a href="http://localhost/grupog2/ingrediente.php">Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/livro.php">Livro</a></li>
				<li><a href="http://localhost/grupog2/medida.php">Medida</a></li>
				<li><a href="http://localhost/grupog2/receita.php">Receita</a></li>
				<li><a href="http://localhost/grupog2/receita_ingrediente_medida.php">Receita Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/receita_publicada.php">Receita Publicada</a></li>
				<li><a href="http://localhost/grupog2/referencia.php">Referência</a></li>
				<li><a href="http://localhost/grupog2/restaurante.php">Restaurante</a></li>
				</ul>
</div>
</nav>

<div class="row container">
    <p>&nbsp;</p>  
		<form action="/action_page.php">
		<fieldset class="ingrediente">
		<legend><img src="imagens/logochef.png" alt="[imagem]" width="100"></legend>
            <h5 class="light center">Medida</h5>
			
			<div class="input-field col s12">
			<input type="text" name="nome_rec" placeholder="Digite o nome da receita"><br><br>
</div>			
			<div class="input-field col s12">
			<input type="text" name="coz_elaborador_rec" placeholder="Digite o nome do cozinheiro elaborador"><br><br>
</div>			
			<div class="input-field col s12">
			<input type="text" name="idIngred_rec" placeholder="Digite o ID do ingrediente"><br><br>
</div>			
			<div class="input-field col s12">
			<input type="text" name="qtde" placeholder="Digite a quantidade"><br><br>
</div>			
			<div class="input-field col s12">
			<input type="text" name="idMedida" placeholder="Digite o ID da medida"><p>&nbsp;</p> 
			
			
            <div class="input-field col s12">
                <input type="submit" value="Cadastrar" class="btn green">
                <input type="submit" value="Limpar" class="btn red">
			</div>
	</div>


</fieldset>
</form>
<script type="text/javascript" scr="materialize/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" scr="materialize/js/materialize.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

    });


		
		</script>
<?php
if(isset($_SESSION['msg_rec_ing'])){
echo $_SESSION['msg_rec_ing'];
unset($_SESSION['msg_rec_ing']);
}
?>
	
		<br><br>
		<?php
		include_once("conexao.php");
		$result_receita_ingrediente_medida = "SELECT * FROM mvgv_receita_ingrediente_medida";
		$resultado_receita_ingrediente_medida = mysqli_query($conn, $result_receita_ingrediente_medida);
		?>
		<div class="page-header">
		<br>
		</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table" border="1">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Cozinheiro</th>
								<th>ID</th>
								<th>Quantidade</th>
								<th>Medida</th>
								<th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<?php while($rows_receita_ingrediente_medida = mysqli_fetch_assoc($resultado_receita_ingrediente_medida)){ ?>
								<tr>
									<td><?php echo $rows_receita_ingrediente_medida ['nome_rec_med']; ?></td>
									<td><?php echo $rows_receita_ingrediente_medida ['coz_elaborador_rec']; ?></td>
									<td><?php echo $rows_receita_ingrediente_medida ['idIngred_rec']; ?></td>
									<td><?php echo $rows_receita_ingrediente_medida ['qtde']; ?></td>
									<td><?php echo $rows_receita_ingrediente_medida ['idMedida']; ?></td>
									<td>
										<button type="button" class="btn btn-xs btn-primary">Visualizar</button>
										<button type="button" class="btn btn-xs btn-warning">Editar</button>
										<button type="button" class="btn btn-xs btn-danger">Apagar</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</center>	
	</body>
</html>