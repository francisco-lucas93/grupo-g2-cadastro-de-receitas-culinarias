<?php 
session_start();
?>
<html>
    <head>
        <meta charset"UTF-8">
        <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--CSS MATERIALIZE-->
      <link rel="stylesheet" href="materialize/css/materialize.min.css">
        <title></title>
    </head>

    <body>

    <nav class="blue-grey">
        <div class="nav-wrapper container">
            <div class="brand-logo light"></div>
            <ul class="right">
			
				<li><a href="http://localhost/grupog2/index.php">HOME</a></li>
				<li><a href="http://localhost/grupog2/cargo.php">Cargo</a></li>
				<li><a href="http://localhost/grupog2/funcionario.php">Funcionário</a></li>
				<li><a href="http://localhost/grupog2/categoria.php">Categoria</a></li>
				<li><a href="http://localhost/grupog2/ingrediente.php">Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/livro.php">Livro</a></li>
				<li><a href="http://localhost/grupog2/medida.php">Medida</a></li>
				<li><a href="http://localhost/grupog2/receita.php">Receita</a></li>
				<li><a href="http://localhost/grupog2/receita_ingrediente_medida.php">Receita Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/receita_publicada.php">Receita Publicada</a></li>
				<li><a href="http://localhost/grupog2/referencia.php">Referência</a></li>
				<li><a href="http://localhost/grupog2/restaurante.php">Restaurante</a></li>
				</ul>
</div>
</nav>

<div class="row container">
    <p>&nbsp;</p>  
	<form method="POST" action="b_dados.php">
		<fieldset class="ingrediente">
		<legend><img src="imagens/logochef.png" alt="[imagem]" width="100"></legend>
            <h5 class="light center">Receita publicada</h5>
			
			<div class="input-field col s12">
			<input type="text" name="idLivro" placeholder="Digite a ID do livro"><br><br>
			</div>

			<div class="input-field col s12">
			<input type="text" name="nome" placeholder="Digite o nome do livro"><br><br>
			</div>
						
			<div class="input-field col s12">
			<input type="text" name="coz_elaborador" placeholder="Digite o cozinheiro"><p>&nbsp;</p> 
			</div>			
			
            <div class="input-field col s12">
                <input type="submit" value="Cadastrar" class="btn green">
                <input type="submit" value="Limpar" class="btn red">
			</div>
	</div>


</fieldset>
</form>

<script type="text/javascript" scr="materialize/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" scr="materialize/js/materialize.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

    });


		
		</script>
			
			<?php
			if(isset($_SESSION['msg_rec_pub'])){
				echo $_SESSION['msg_rec_pub'];
				unset($_SESSION['msg_rec_pub']);
			}
			?>
		
		<br>
		<?php
		include_once("conexao.php");
		$result_receita_publicada = "SELECT * FROM mvgv_receita_publicada";
		$resultado_receita_publicada = mysqli_query($conn, $result_receita_publicada);
		?>
		<div class="page-header">
		<br>
		</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table" border="1">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nome</th>
								<th>Colaborador</th>
								<th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<?php while($rows_receita_publicada = mysqli_fetch_assoc($resultado_receita_publicada)){ ?>
								<tr>
									<td><?php echo $rows_receita_publicada ['idLivro']; ?></td>
									<td><?php echo $rows_receita_publicada ['nome']; ?></td>
									<td><?php echo $rows_receita_publicada ['coz_elaborador']; ?></td>
									<td>
										<button type="button" class="btn btn-xs btn-primary">Visualizar</button>
										<button type="button" class="btn btn-xs btn-warning">Editar</button>
										<button type="button" class="btn btn-xs btn-danger">Apagar</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</center>	
	</body>
</html>