<?php 
session_start();
?>
<html>
    <head>
        <meta charset"UTF-8">
        <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--CSS MATERIALIZE-->
      <link rel="stylesheet" href="materialize/css/materialize.min.css">
        <title></title>
    </head>

    <body>

    <nav class="blue-grey">
        <div class="nav-wrapper container">
            <div class="brand-logo light"></div>
            <ul class="right">
			
				<li><a href="http://localhost/grupog2/index.php">HOME</a></li>
				<li><a href="http://localhost/grupog2/cargo.php">Cargo</a></li>
				<li><a href="http://localhost/grupog2/funcionario.php">Funcionário</a></li>
				<li><a href="http://localhost/grupog2/categoria.php">Categoria</a></li>
				<li><a href="http://localhost/grupog2/ingrediente.php">Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/livro.php">Livro</a></li>
				<li><a href="http://localhost/grupog2/medida.php">Medida</a></li>
				<li><a href="http://localhost/grupog2/receita.php">Receita</a></li>
				<li><a href="http://localhost/grupog2/receita_ingrediente_medida.php">Receita Ingrediente</a></li>
				<li><a href="http://localhost/grupog2/receita_publicada.php">Receita Publicada</a></li>
				<li><a href="http://localhost/grupog2/referencia.php">Referência</a></li>
				<li><a href="http://localhost/grupog2/restaurante.php">Restaurante</a></li>
				</ul>
</div>
</nav>

<div class="row container">
    <p>&nbsp;</p>  
	<form method="POST" action="b_dados.php">
			
		<fieldset class="ingrediente">
		<legend><img src="imagens/logochef.png" alt="[imagem]" width="100"></legend>
            <h5 class="light center">Referência</h5>
			
			<div class="input-field col s12">
			<input type="text" name="idFunc" placeholder="Digite a ID do funcionário"><br><br>
			</div>

			<div class="input-field col s12">
			<input type="text" name="idRestaurante" placeholder="Digite a ID do restaurante"><br><br>
</div>

			<div class="input-field col s12">
			<label>Data de início: </label>
</div>
			<div class="input-field col s12">
			<input type="date" name="data_inicio" placeholder="Digite a data de inicio"><br><br>
</div>

			<div class="input-field col s12">
			<label>Data de fim: </label>
</div>
			<div class="input-field col s12">
			<input type="date" name="data_fim" placeholder="Digite a data de fim"><p>&nbsp;</p> 
			</div>			
			
            <div class="input-field col s12">
                <input type="submit" value="Cadastrar" class="btn green">
                <input type="submit" value="Limpar" class="btn red">
			</div>
	</div>


</fieldset>
</form>

<script type="text/javascript" scr="materialize/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" scr="materialize/js/materialize.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

    });


		
		</script>
			
			<?php
			if(isset($_SESSION['msg_ref'])){
				echo $_SESSION['msg_ref'];
				unset($_SESSION['msg_ref']);
			}
			?>
		</form>
		<?php
		include_once("conexao.php");
		$result_referencia = "SELECT * FROM mvgv_referencia";
		$resultado_referencia = mysqli_query($conn, $result_referencia);
		?>
		<div class="page-header">
		<br>
		</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table" border="1">
						<thead>
							<tr>
								<th>ID</th>
								<th>Restaurante</th>
								<th>Data de início</th>
								<th>Data de fim</th>
								<th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<?php while($rows_referencia = mysqli_fetch_assoc($resultado_referencia)){ ?>
								<tr>
									<td><?php echo $rows_referencia ['idFunc']; ?></td>
									<td><?php echo $rows_referencia ['idRestaurante']; ?></td>
									<td><?php echo $rows_referencia ['data_inicio']; ?></td>
									<td><?php echo $rows_referencia ['data_fim']; ?></td>
									<td>
										<button type="button" class="btn btn-xs btn-primary">Visualizar</button>
										<button type="button" class="btn btn-xs btn-warning">Editar</button>
										<button type="button" class="btn btn-xs btn-danger">Apagar</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</center>	
	</body>
</html>